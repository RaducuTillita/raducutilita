package homeworkweek05.domain;

public class Student extends Person{

    public Student(String name, int age) {
        super( name, age );
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
