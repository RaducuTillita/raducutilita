package homeworkweek05.domain.comparator;

import homeworkweek05.domain.Person;

import java.util.Comparator;

public class MyNameComparator implements Comparator<Person> {

    public int compare(Person p1, Person p2)
    {
        return p1.getName().compareTo(p2.getName());
    }

}
