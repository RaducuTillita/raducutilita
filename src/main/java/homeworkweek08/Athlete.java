package homeworkweek08;

import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Athlete {

    private int number;
    private String athleteName;
    private CountryCode countryCode;
    private double skyTimeResult;
    private String firstShootingRange;
    private String secondShootingRange;
    private String thirdShootingRange;
    private double misses;
    private double finalTime = 0 ;


    public Athlete(int number, String athleteName, CountryCode countryCode, double skyTimeResult, String firstShootingRange, String secondShootingRange, String thirdShootingRange) {
        this.number = number;
        this.athleteName = athleteName;
        this.countryCode = countryCode;
        this.skyTimeResult = skyTimeResult;
        this.firstShootingRange = firstShootingRange;
        this.secondShootingRange = secondShootingRange;
        this.thirdShootingRange = thirdShootingRange;
    }

    public double misses() {
        int countFirst = StringUtils.countMatches( firstShootingRange, 'o' );
        int countSecond = StringUtils.countMatches( secondShootingRange, 'o' );
        int countThird = StringUtils.countMatches( thirdShootingRange, 'o' );
        return misses = (countFirst + countSecond + countThird) *10;
    }

    public double finalTime(){
        int countFirst = StringUtils.countMatches( firstShootingRange, 'o' );
        int countSecond = StringUtils.countMatches( secondShootingRange, 'o' );
        int countThird = StringUtils.countMatches( thirdShootingRange, 'o' );
        double count = countFirst + countSecond + countThird;

        finalTime = Double.parseDouble( String.valueOf( skyTimeResult + (count*0.1) ) );

        return finalTime;
    }



    public int getNumber() {
        return number;
    }

    public String getAthleteName() {
        return athleteName;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public double getSkyTimeResult() {
        return skyTimeResult;
    }

    public String getFirstShootingRange() {
        return firstShootingRange;
    }

    public String getSecondShootingRange() {
        return secondShootingRange;
    }

    public String getThirdShootingRange() {
        return thirdShootingRange;
    }

    @Override
    public String toString() {
        return
                " number=" + number +
                ", athleteName='" + athleteName + '\'' +
                ", countryCode=" + countryCode +
                ", skyTimeResult='" + skyTimeResult + '\'' +
                ", firstShootingRange='" + firstShootingRange + '\'' +
                ", secondShootingRange='" + secondShootingRange + '\'' +
                ", thirdShootingRange='" + thirdShootingRange + '\'' +
                "\n";
    }
}
