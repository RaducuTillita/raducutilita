package homeworkweek10;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Path path = Paths.get( "src/main/resources/random_names_fossbytes.csv" );

        String line = "";
        BufferedReader bufferedReader = null;

        List<Person> persons = new ArrayList<>();
        TreeSet<Person> orderedPersons = new TreeSet<>( new ComparatorPerson() );

        File file = new File( "src/main/resources/random_names_fossbytes.csv" );

        try {

            bufferedReader = new BufferedReader( new FileReader( file ) );

            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split( "," );
                System.out.println( "First name : " + values[0] + " , Last Name : " + values[1] + " , Birthday : " + values[2] );

                if (values[0] != null && values[1] != null) {
                    Person person = new Person( values[0], values[1], values[2] );
                    persons.add( person );
                }
            }
        } catch (IOException e) {
            System.out.println( e );
        }

        System.out.println();
        System.out.println( "Input birthday : " );
        Scanner scanner = new Scanner( System.in );
        String inputBirthDate = scanner.nextLine();


        Set<Person> lines = persons.stream()
                .filter( u -> (u.getBirthDay().compareTo( inputBirthDate ) == 0) )
                .collect( Collectors.toSet() );
        orderedPersons.addAll( lines );

        try {
            writeToCSV( orderedPersons );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeToCSV(Set<Person> sorted) throws Exception {
        try (BufferedWriter writer = new BufferedWriter( new OutputStreamWriter( new FileOutputStream( "src/main/java/homeworkweek10/PersonBirthDateSorted.csv" ), StandardCharsets.ISO_8859_1 ) )) {
            for (Person line : sorted) {
                writer.write( toCSVLine( line ) );

            }
        }
    }

    public static String toCSVLine(Person person) {
        return person.getFirstName() + "," + person.getLastName() + "," + person.getBirthDay() + "\n";
    }
}




