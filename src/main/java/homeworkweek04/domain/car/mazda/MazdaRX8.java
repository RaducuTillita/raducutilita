package homeworkweek04.domain.car.mazda;

public class MazdaRX8 extends Mazda {

    public MazdaRX8() {
        System.out.println("A new RX-8 has been instantiated.");
    }

    public MazdaRX8(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );
    }
}
