package homeworkweek04.domain.car.dacia;

import homeworkweek04.domain.car.Car;

public class DaciaLogan extends Dacia {


    public DaciaLogan() {
        System.out.println("A new Dacia Logan has been instantiated.");
    }

    public DaciaLogan(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );

    }

    @Override
    public void drive(float distance) {
        super.drive( distance );

    }


}
