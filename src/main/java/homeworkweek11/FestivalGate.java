package homeworkweek11;

public class FestivalGate {

    int numberFullTickets;
    int numberFullVipTickets;
    int numberFreePassTickets;
    int numberOneDayTickets;
    int numberOneDayVipTickets;
    int totalNumberOfTickets;

    public static int totalNumberOfThreads = 100;

    public FestivalGate() {
        this.numberFullTickets = 0;
    }

    public void open(){
        for (int i = 1; i <= totalNumberOfThreads; i++) {
            MyThread myThread = new MyThread();
            myThread.start();

            try {
                myThread.join();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public int getNumberFullTickets() {
        return numberFullTickets;
    }

    public int getNumberFullVipTickets() {
        return numberFullVipTickets;
    }

    public int getNumberFreePassTickets() {
        return numberFreePassTickets;
    }

    public int getNumberOneDayTickets() {
        return numberOneDayTickets;
    }

    public int getNumberOneDayVipTickets() {
        return numberOneDayVipTickets;
    }


    public int getTotalNumberOfTickets() {
        return totalNumberOfTickets;
    }

    public synchronized void addOneFullTickets() {
        this.numberFullTickets = getNumberFullTickets() + 1;
        addTotalNumberOfTickets();
    }

    public synchronized void addOneFullVipTickets() {
        this.numberFullVipTickets = getNumberFullVipTickets() + 1;
        addTotalNumberOfTickets();
    }

    public synchronized void addOneFreePassTickets() {
        this.numberFreePassTickets = getNumberFreePassTickets() + 1;
        addTotalNumberOfTickets();
    }

    public synchronized void addOneDayTickets() {
        this.numberOneDayTickets = getNumberOneDayTickets() + 1;
        addTotalNumberOfTickets();
    }

    public synchronized void addOneDayVipTickets() {
        this.numberOneDayVipTickets = getNumberOneDayVipTickets() + 1;
        addTotalNumberOfTickets();
    }

    public synchronized void addTotalNumberOfTickets() {
        this.totalNumberOfTickets = getTotalNumberOfTickets() + 1;
    }

    public void addTicket(MyThread.TicketType ticketType) {
        if (ticketType == MyThread.TicketType.fullTickets) {
            addOneFullTickets();
        } else if (ticketType == MyThread.TicketType.fullVIPPasses) {
            addOneFullVipTickets();
        } else if (ticketType == MyThread.TicketType.freePasses) {
            addOneFreePassTickets();
        } else if (ticketType == MyThread.TicketType.oneDayPasses) {
            addOneDayTickets();
        } else if (ticketType == MyThread.TicketType.oneDayVIPPasses) {
            addOneDayVipTickets();
        }
    }

}
