package homeworkweek09;

import java.util.*;

public class Repo {

    List<Student> students = new ArrayList<>();


    public void addStudents(String ID, Student.Gender gender, String firstName, String lastName, int birthDate) {
        Student student = null;
        try {
            student = new Student( ID, gender, firstName, lastName, birthDate );

        } catch (Student.InvalidNameException e) {
            System.err.println( "cannot introduce student in repository system because of either age limit error" );
        }

        if (student.getBirthDate() <= 1900) {
            System.err.println( "Student birthdate should be higher than 1900 : " + student );
        } else if (student.getBirthDate() > 2004) {
            System.err.println( "Person too young to be a student : " + student );
        } else if (student.getFirstName() == "") {
            System.err.println( "Field is empty in first name " +student );
        }else  if (student.getLastName() == "") {
            System.err.println( "Field is empty in last name " + student );
        }else {
        students.add(student);
        addStudentToTreeSetRepo(student);}
    }



    public boolean removeStudents(String ID) {


        Student student = null;
        try {
            if (ID.equals("")) {
                throw new IllegalArgumentException("Cannot remove any student if the ID is null");
            } else {

                for (Student id : students) {
                    if (id.getID().equals(ID)) {
                        student = id;
                    }
                }
                if (student != null) {
                    students.remove(student);
                }
                return true;
            }


        } catch (IllegalArgumentException e) {
            System.err.println("ID-input for student to be removed does not exist in repository or student is missing altogether");
        }
        return false;
    }

    public Student retrieveStudents(int age, int birthDate) {

        try {

            for (Student agee : students) {
                int studentAge = 2022 - birthDate;
                if (studentAge == age) {
                    return agee;

                }
            }

        } catch (NumberFormatException e) {
            System.err.println("Age input needs to be an integer");
        } catch (IllegalArgumentException e) {
            System.err.println("Age input needs to be a positive number");
        }
        return null;
    }


    public boolean addStudentToTreeSetRepo(Student student) {

        try {

            Comparator<Student> studentComparator = new Comparator<Student>() {
                @Override
                public int compare(Student o1, Student o2) {

                    return o1.getBirthDate()-(o2.getBirthDate());

                }
            };

            Set<Student> sortedStudents = new TreeSet<>(studentComparator);
            sortedStudents.add(student);
            return true;

        } catch (IllegalArgumentException e) {

            System.err.println("Cannot introduce student in ordered set within repository system because birth date is missing");
        }

        return false;
    }


    @Override
    public String toString() {
        return "Repo{" +
                "students=" + students +
                '}';
    }
}