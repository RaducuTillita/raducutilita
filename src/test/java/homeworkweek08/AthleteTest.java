package homeworkweek08;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AthleteTest {

    private Athlete athlete;

    @BeforeEach
    void setUp(){athlete = new Athlete( 11, "Umar Jorgson", CountryCode.SLOVAKIA, 30.27, "xxxox", "xxxxx", "xxoxo" );}
    @Test
    void finishTime_misses_finalTime(){
        int countFirst = StringUtils.countMatches( athlete.getFirstShootingRange(), 'o' );
        int countSecond = StringUtils.countMatches( athlete.getSecondShootingRange(), 'o' );
        int countThird = StringUtils.countMatches( athlete.getThirdShootingRange(), 'o' );
        double count = countFirst + countSecond + countThird;
        double finalTime = Double.parseDouble( String.valueOf( athlete.getSkyTimeResult() + (count*0.1) ) );

        assertEquals( 30.57,finalTime );
    }

}